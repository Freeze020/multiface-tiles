async function imageLoader(tileId, sheet) {
  let tile = canvas.scene.tiles.get(tileId);
  let pickedFile = await new FilePicker({
    type: "imagevideo",
    callback: async (path) => {
      let paths = tile.getFlag("multiface-tiles", "altImages") || [];
      paths.push(path);
      await tile.setFlag("multiface-tiles", "altImages", paths);
      sheet.render();
    }
  });
  pickedFile.browse();
}

async function changeImage(path, tileId) {
  let currentPath = canvas.scene.tiles.get(tileId).texture.src;
  if (currentPath === path) return;
  await canvas.scene.updateEmbeddedDocuments("Tile", [{ _id: tileId, "texture.src": path }]);
}

async function changeImageV9(path, tileId) {
  let currentPath = canvas.scene.data.tiles.get(tileId).data.img;
  if (currentPath === path) return;
  await canvas.scene.updateEmbeddedDocuments("Tile", [{ _id: tileId, img: path }]);
}

function createChild({ classes = "", id = null, style = null } = {}, { childNode = null, title = "", childClasses = "" } = {}) {
  const wrapper = document.createElement("div");
  if (id) wrapper.id = id;
  if (style) wrapper.style = style;
  if (childNode) {
    const child = document.createElement(childNode);
    child.title = title;
    for (let e of childClasses.split(" ")) child.classList.add(e);
    wrapper.appendChild(child);
  }
  for (let e of classes.split(" ")) wrapper.classList.add(e);
  return wrapper;
}
function createImageChild({ src = "", width = 0, height = 0, title = "" } = {}) {
  const img = document.createElement("img");
  img.src = src;
  img.height = height;
  img.width = width;
  img.title = title;
  return img;
}

Hooks.on("renderTileHUD", async function (sheet, [html], data) {
  const dimension = foundry.utils.isNewerVersion("12", game.version) ? Math.clamped(1 / canvas.scene._viewPosition.scale, 1, 3.5) : Math.clamp(1 / canvas.scene._viewPosition.scale, 1, 3.5);
  if (foundry.utils.isNewerVersion("10", game.version)) {
    const tile = sheet.object;
    let originalImage = tile.document.getFlag("multiface-tiles", "originalImage");
    let altImages = tile.document.getFlag("multiface-tiles", "altImages") || [];
    const hasAltImages = altImages.length !== 0;
    if (!originalImage) {
      await tile.document.setFlag("multiface-tiles", "originalImage", tile.data.img);
      originalImage = tile.data.img;
    }
    if (originalImage !== tile.data.img && !altImages.includes(tile.data.img)) {
      await tile.document.setFlag("multiface-tiles", "originalImage", tile.data.img);
      originalImage = tile.data.img;
    }
    html.querySelector("#tile-hud .col.middle").appendChild(createChild({ classes: "multiface-tiles control-box" }));
    html.querySelector("#tile-hud .col.middle .control-box").appendChild(createChild({ classes: "multiface-tiles control-icon", id: "multiface-tiles-alternative-image-add", style: `margin-right: ${hasAltImages ? 10 : 0}px;transform: scale(${Math.min(dimension, 1.5)});` }, { childNode: "i", childClasses: "far fa-plus-square", title: game.i18n.format('MULTIFACETILES.tooltip1') }))
    html.querySelector("#multiface-tiles-alternative-image-add").addEventListener("click", async function () {
      await imageLoader(tile.id, sheet);
    });

    if (altImages.length > 0) {
      const active = !!tile.document.getFlag("multiface-tiles", "imageRemove") ? " active" : "";
      html.querySelector("#tile-hud .col.middle .control-box").appendChild(createChild({ classes: `multiface-tiles control-icon${active}`, id: "multiface-tiles-alternative-image-remove", style: `margin-left: ${hasAltImages ? 10 : 0}px;transform: scale(${Math.min(dimension, 1.5)}` }, { childNode: "i", childClasses: "far fa-minus-square", title: game.i18n.format('MULTIFACETILES.tooltip5') }));
      html.querySelector("#multiface-tiles-alternative-image-remove").addEventListener("click", async function () {
        if (Array.from(this.classList).includes("active")) {
          this.classList.remove("active");
          await tile.document.unsetFlag("multiface-tiles", "imageRemove");
          for (let i = 0; i < altImages.length; i++) {
            html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).classList.remove("active");
          }
        }
        else {
          this.classList.add("active");
          await tile.document.setFlag("multiface-tiles", "imageRemove", true);
          for (let i = 0; i < altImages.length; i++) {
            html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).classList.add("active");
          }
        }
      });
    }
    html.querySelector("#tile-hud .col.middle").appendChild(createChild({ classes: "multiface-tiles image-box" }));
    if (altImages.length > 0) {
      for (let i = 0; i < altImages.length; i++) {
        html.querySelector("#tile-hud .col.middle .image-box").appendChild(createChild({ classes: "multiface-tiles control-icon", id: `multiface-tiles-alternative-image-${i + 1}`, style: `margin: 0px ${dimension * 14}px; transform: scale(${dimension}) translateY(${-dimension * 5}px);` })).appendChild(createImageChild({ src: `${altImages[i].toLowerCase().includes(".webm") ? "icons/svg/video.svg" : altImages[i]}`, width: 36, height: 36, title: `${game.i18n.format('MULTIFACETILES.tooltip3')} ${altImages[i].substring(altImages[i].lastIndexOf("/") + 1)}` }));
        if (Array.from(html.querySelector("#multiface-tiles-alternative-image-remove").classList).includes("active")) html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).classList.add("active");
        html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).addEventListener("click", async function () {
          if (Array.from(html.querySelector("#multiface-tiles-alternative-image-remove").classList).includes("active")) {
            const key = altImages.indexOf(altImages.find(alt => alt === altImages[i]));
            const newList = altImages.filter((_, n) => n !== key);
            console.log(newList)
            await tile.document.setFlag("multiface-tiles", "altImages", newList);
            sheet.render();
            if (newList.length === 0) await tile.document.unsetFlag("multiface-tiles", "imageRemove");
          }
          else {
            changeImage(altImages[i], tile.id);
            for (let i = 0; i < altImages.length; i++) {
              html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).classList.remove("active");
            }
            html.querySelector(`#multiface-tiles-alternative-image-original`).classList.remove("active")
            this.classList.add("active");
          }
        })
      }
    }
    html.querySelector("#tile-hud .col.middle .image-box").appendChild(createChild({ classes: "multiface-tiles control-icon", id: "multiface-tiles-alternative-image-original", style: `margin: 0px ${dimension * 14}px; transform: scale(${dimension}) translateY(${-dimension * 5}px);` })).appendChild(createImageChild({ src: `${originalImage.toLowerCase().includes(".webm") ? "icons/svg/video.svg" : originalImage}`, width: 36, height: 36, title: `${game.i18n.format('MULTIFACETILES.tooltip4')}` }));
    html.querySelector(`#multiface-tiles-alternative-image-original`).addEventListener("click", async function () {
      changeImageV9(originalImage, tile.id);
      this.classList.add("active");
      for (let i = 0; i < altImages.length; i++) {
        html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).classList.remove("active");
      }
    });
    const currentImage = tile.data.img;
    const indexOfImage = altImages.indexOf(currentImage);
    if (indexOfImage < 0) html.querySelector(`#multiface-tiles-alternative-image-original`).classList.add("active");
    else {
      html.querySelector(`#multiface-tiles-alternative-image-${indexOfImage + 1}`).classList.add("active");
    }
  }
  if (foundry.utils.isNewerVersion(game.version, "10")) {
    const tile = sheet.object;
    let originalImage = tile.document.getFlag("multiface-tiles", "originalImage");
    let altImages = tile.document.getFlag("multiface-tiles", "altImages") || [];
    const hasAltImages = altImages.length !== 0;
    if (!originalImage) {
      await tile.document.setFlag("multiface-tiles", "originalImage", tile.document.texture.src);
      originalImage = tile.document.texture.src;
    }
    if (originalImage !== tile.document.texture.src && !altImages.includes(tile.document.texture.src)) {
      await tile.document.setFlag("multiface-tiles", "originalImage", tile.document.texture.src);
      originalImage = tile.document.texture.src;
    }
    html.querySelector("#tile-hud .col.middle").appendChild(createChild({ classes: "multiface-tiles control-box" }));
    html.querySelector("#tile-hud .col.middle .control-box").appendChild(createChild({ classes: "multiface-tiles control-icon", id: "multiface-tiles-alternative-image-add", style: `margin-right: ${hasAltImages ? 10 : 0}px;transform: scale(${Math.min(dimension, 1.5)});` }, { childNode: "i", childClasses: "far fa-plus-square", title: game.i18n.format('MULTIFACETILES.tooltip1') }))
    html.querySelector("#multiface-tiles-alternative-image-add").addEventListener("click", async function () {
      await imageLoader(tile.id, sheet);
    });

    if (altImages.length > 0) {
      const active = !!tile.document.getFlag("multiface-tiles", "imageRemove") ? " active" : "";
      html.querySelector("#tile-hud .col.middle .control-box").appendChild(createChild({ classes: `multiface-tiles control-icon${active}`, id: "multiface-tiles-alternative-image-remove", style: `margin-left: ${hasAltImages ? 10 : 0}px;transform: scale(${Math.min(dimension, 1.5)}` }, { childNode: "i", childClasses: "far fa-minus-square", title: game.i18n.format('MULTIFACETILES.tooltip5') }));
      html.querySelector("#multiface-tiles-alternative-image-remove").addEventListener("click", async function () {
        if (Array.from(this.classList).includes("active")) {
          this.classList.remove("active");
          await tile.document.unsetFlag("multiface-tiles", "imageRemove");
          for (let i = 0; i < altImages.length; i++) {
            html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).classList.remove("active");
          }
        }
        else {
          this.classList.add("active");
          await tile.document.setFlag("multiface-tiles", "imageRemove", true);
          for (let i = 0; i < altImages.length; i++) {
            html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).classList.add("active");
          }
        }
      });
    }
    html.querySelector("#tile-hud .col.middle").appendChild(createChild({ classes: "multiface-tiles image-box" }));
    if (altImages.length > 0) {
      for (let i = 0; i < altImages.length; i++) {
        html.querySelector("#tile-hud .col.middle .image-box").appendChild(createChild({ classes: "multiface-tiles control-icon", id: `multiface-tiles-alternative-image-${i + 1}`, style: `margin: 0px ${dimension * 14}px; transform: scale(${dimension}) translateY(${-dimension * 5}px);` })).appendChild(createImageChild({ src: `${altImages[i].toLowerCase().includes(".webm") ? "icons/svg/video.svg" : altImages[i]}`, width: 36, height: 36, title: `${game.i18n.format('MULTIFACETILES.tooltip3')} ${altImages[i].substring(altImages[i].lastIndexOf("/") + 1)}` }));
        if (Array.from(html.querySelector("#multiface-tiles-alternative-image-remove").classList).includes("active")) html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).classList.add("active");
        html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).addEventListener("click", async function () {
          if (Array.from(html.querySelector("#multiface-tiles-alternative-image-remove").classList).includes("active")) {
            const key = altImages.indexOf(altImages.find(alt => alt === altImages[i]));
            const newList = altImages.filter((_, n) => n !== key);
            console.log(newList)
            await tile.document.setFlag("multiface-tiles", "altImages", newList);
            sheet.render();
            if (newList.length === 0) await tile.document.unsetFlag("multiface-tiles", "imageRemove");
          }
          else {
            changeImage(altImages[i], tile.id);
            for (let i = 0; i < altImages.length; i++) {
              html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).classList.remove("active");
            }
            html.querySelector(`#multiface-tiles-alternative-image-original`).classList.remove("active")
            this.classList.add("active");
          }
        })
      }
    }
    html.querySelector("#tile-hud .col.middle .image-box").appendChild(createChild({ classes: "multiface-tiles control-icon", id: "multiface-tiles-alternative-image-original", style: `margin: 0px ${dimension * 14}px; transform: scale(${dimension}) translateY(${-dimension * 5}px);` })).appendChild(createImageChild({ src: `${originalImage.toLowerCase().includes(".webm") ? "icons/svg/video.svg" : originalImage}`, width: 36, height: 36, title: `${game.i18n.format('MULTIFACETILES.tooltip4')}` }));
    html.querySelector(`#multiface-tiles-alternative-image-original`).addEventListener("click", async function () {
      changeImage(originalImage, tile.id);
      this.classList.add("active");
      for (let i = 0; i < altImages.length; i++) {
        html.querySelector(`#multiface-tiles-alternative-image-${i + 1}`).classList.remove("active");
      }
    });
    const currentImage = tile.document.texture.src;
    const indexOfImage = altImages.indexOf(currentImage);
    if (indexOfImage < 0) html.querySelector(`#multiface-tiles-alternative-image-original`).classList.add("active");
    else {
      html.querySelector(`#multiface-tiles-alternative-image-${indexOfImage + 1}`).classList.add("active");
    }
  }
});

Hooks.on("canvasReady", async function (canvas) {
  const tileTextures = canvas.scene.tiles.filter(t => t.getFlag("multiface-tiles", "altImages")).reduce((acc, t) => {
    const paths = t.getFlag("multiface-tiles", "altImages");
    for (let path of paths) acc.add(path)
    return acc;
  }, new Set());
  await TextureLoader.loader.load([...tileTextures]);
});