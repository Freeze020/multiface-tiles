Change log:

- 1.0.2 fixes a small oversight that when the tile image has been changed with the Tile configuration settings, it would ignore that change for the original image icon, which would reverse the change the user would have made.
- 1.0.3 minor fixes.
- 1.0.4 adds V10 compatibility. And improved namespacing of html elements.
- 1.0.5 was still using .img for texture source of a tile, should have been using texture.src in v10. Fixed now.
- 1.0.6 Added a "subtle" HUD scaling so when zoomed out the hud icons are a bit more visible. Especially helpful for full canvas tiles.
- 1.0.7 Caching of images introduced, and more v10 fixes.
- 1.0.8 caching images/webM a bit more neatly. Now added scaling of buttons to v10.
- 1.0.12 made the module.json compatible with v12.
- 1.0.12.1 fixed a few minor issues and refactored the module away from jquery.